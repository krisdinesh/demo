import { AngularKarmaJasminePage } from './app.po';

describe('angular-karma-jasmine App', () => {
  let page: AngularKarmaJasminePage;

  beforeEach(() => {
    page = new AngularKarmaJasminePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to Angular Unit Testing!');
  });

  it('Should calculate 20', () => {
    page.navigateTo();
    expect(20).toEqual(20);
  });

  it('Should calculate 30', () => {
    page.navigateTo();
    expect(30).toEqual(30);
  });

  it('Should calculate 40', () => {
    page.navigateTo();
    expect(40).toEqual(40);
  });

  it('Should calculate 50', () => {
    page.navigateTo();
    expect(50).toEqual(50);
  });

});
