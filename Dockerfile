FROM node:latest

# add ascend certificate
RUN curl -s http://proget.ascendlearning.com/endpoints/common/content/certs/zscaler_root.crt --output "/usr/local/share/ca-certificates/zscaler_root.crt" \
    && chmod 644 /usr/local/share/ca-certificates/zscaler_root.crt \
    && update-ca-certificates
RUN npm config set registry http://registry.npmjs.org/

# Setting up JAVA
RUN apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get clean;

RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

# Setup JAVA_HOME -- useful for docker commandline
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

# install and cache app dependencies
# RUN npm install
ENV NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt
RUN npm config set cafile /etc/ssl/certs/ca-certificates.crt
RUN npm install -g @angular/cli


# install gulp
RUN npm install gulp-cli -g
RUN npm install gulp -D

# install protractor
RUN npm install -g protractor

# install webdriver-manager
RUN npm install webdriver-manager

# Install Chrome
RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list

# RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN wget --no-check-certificate -qO - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN set -x \
    && apt-get update \
    && apt-get install -y \
        google-chrome-stable

# This line is to tell karma-chrome-launcher where chrome is available
ENV CHROME_BIN /usr/bin/google-chrome

# Log versions
RUN set -x \
    && node -v \
    && npm -v \
    && google-chrome --version
